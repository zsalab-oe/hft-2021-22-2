# Haladó fejlesztési technikák
--------------

A Haladó fejlesztési technikák laborokon ismertetett anyagok kódjai és példái.

## Letöltés

* .zip letöltés: Downloads >> Download repository
* cli: `git clone https://zsalab-oe@bitbucket.org/zsalab-oe/hft-2021-22-2.git`

## Hasznos linkek

* Az anyagok megtalálhatók a [http://users.nik.uni-obuda.hu/zsalab/hft/](http://users.nik.uni-obuda.hu/zsalab/hft/) oldalamon is.
* Felvezető videók és labordiák a tantárgy honlapján: [https://nikprog.hu/courses/halado-fejlesztesi-technikak/](https://nikprog.hu/courses/halado-fejlesztesi-technikak/)

## Kapcsolat

Kérdés, óhaj, sóhaj, panasz esetén bátran keressetek a `gaspar.balazs {at} nik.uni-obuda.hu` e-mail címen!

-----------------
*Gáspár Balázs*  
*Óbudai Egyetem Neumann János Informatikai Kar*  
*Szoftvertervezés és -fejlesztés Intézet*  
*2022*