﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Feedback
{
    class Feedback
    {
        public FeedbackCategory Category { get; set; }
        public string Text { get; set; }

        public Feedback(FeedbackCategory category, string text)
        {
            Category = category;
            Text = text;
        }

        public override string ToString()
        {
            return $"[{Category}] - {Text}";
        }
    }
}
