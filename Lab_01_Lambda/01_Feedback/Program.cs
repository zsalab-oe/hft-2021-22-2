﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace _01_Feedback
{
    class Program
    {
        static void Main(string[] args)
        {
            FeedbackManager fm = new FeedbackManager();

            // oldschool
            fm.AddFeedbackProcessor(FeedbackCategory.Bugreport, LogToConsoleBugreport);

            // névtelen metódus
            fm.AddFeedbackProcessor(FeedbackCategory.FeatureRequest, delegate (Feedback feedback)
            {
                ConsoleColor originalColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(feedback);
                Console.ForegroundColor = originalColor;
            });

            // Actiont elmenthetünk egy változóba is - bár nem feltétlenül van értelme.
            Action<Feedback> action = feedback =>
            {
                ConsoleColor originalColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(feedback);
                Console.ForegroundColor = originalColor;
            };

            fm.AddFeedbackProcessor(FeedbackCategory.Opinion, action);


            // lambda kifejezés
            string filePath = "bugreports.txt";
            fm.AddFeedbackProcessor(FeedbackCategory.Bugreport, (feedback) => File.AppendAllText(filePath, feedback.ToString() + "\r\n"));

            //filePath = "featureRequests.txt"; // Outer Variable Trap!!! Inkább használjunk új stringet! (+ A delegáltban használt külső változó lehet konstans - ezzel magunkat is figyelmeztetve, hogy ne írjuk felül a lambda kifejezésben is használt külső változót.)
            string featuresFilePath = "featureRequests.txt";

            fm.AddFeedbackProcessor(FeedbackCategory.FeatureRequest, (feedback) => File.AppendAllText(featuresFilePath, feedback.ToString() + "\r\n"));

            List<Feedback> opinions = new List<Feedback>();
            fm.AddFeedbackProcessor(FeedbackCategory.Opinion, feedback =>
            {
                if (!opinions.Contains(feedback))
                    opinions.Add(feedback);
            });


            fm.AddFeedback(new Feedback(FeedbackCategory.Bugreport, "Valami nem jó."));
            fm.AddFeedback(new Feedback(FeedbackCategory.Opinion, "Tetszik."));
            fm.AddFeedback(new Feedback(FeedbackCategory.FeatureRequest, "Valami hiányzik."));
            fm.AddFeedback(new Feedback(FeedbackCategory.Opinion, "Egész jó."));
            fm.AddFeedback(new Feedback(FeedbackCategory.Bugreport, "Valami megint nem jó."));
            fm.AddFeedback(new Feedback(FeedbackCategory.Bugreport, "Valami még mindig nem jó."));
            fm.AddFeedback(new Feedback(FeedbackCategory.Opinion, "Magánvélemény."));
            fm.AddFeedback(new Feedback(FeedbackCategory.Opinion, "Érezzük, hogy ez komolytalan."));
            fm.AddFeedback(new Feedback(FeedbackCategory.FeatureRequest, "Az kéne, hogy ... tegnapra."));

            ;
            Console.ReadLine();
        }

        static void LogToConsoleBugreport(Feedback feedback)
        {
            ConsoleColor originalColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(feedback);
            Console.ForegroundColor = originalColor;
        }
    }
}
