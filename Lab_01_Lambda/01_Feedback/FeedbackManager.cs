﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Feedback
{
    class FeedbackManager
    {
        const int PROCESS_LIMIT = 3;

        List<Feedback> feedbacks;
        Dictionary<FeedbackCategory, Action<Feedback>> feedbackProcessors;

        public FeedbackManager()
        {
            this.feedbacks = new List<Feedback>();
            this.feedbackProcessors = new Dictionary<FeedbackCategory, Action<Feedback>>();

            //foreach (FeedbackCategory category in Enum.GetValues(typeof(FeedbackCategory)))
            //    this.feedbackProcessors[category] = null;
        }

        public void AddFeedback(Feedback newFeedback)
        {
            feedbacks.Add(newFeedback);
            if (feedbacks.Count >= PROCESS_LIMIT)
            {
                OnProcessAllFeedback();
                Console.WriteLine("!!! FEEDBACKS PROCESSED !!!\n");
                feedbacks.Clear();
            }
        }

        void OnProcessAllFeedback()
        {
            //// oldschool
            //foreach (var f in feedbacks)
            //{
            //    feedbackProcessors[f.Category]?.Invoke(f);
            //}

            //// anomymous method
            //feedbacks.ForEach(delegate (Feedback f) { feedbackProcessors[f.Category]?.Invoke(f); });

            // lambda
            feedbacks.ForEach(f => feedbackProcessors[f.Category]?.Invoke(f));

            // Megjegyzés: azt az esetet nem kezeljük le, ha a dictionaryben nincs az adott visszajelzés kategóriájának megfelelő kulcsú elem. Akit ez zavar (jogosan), végezzen előtte egy ellenőrzést, vagy a konstruktorban minden kategóriához vegyen fel egy null értéket.
        }

        public void AddFeedbackProcessor(FeedbackCategory category, Action<Feedback> processor)
        {
            if (processor == null)
                throw new ArgumentNullException(nameof(processor));

            //if (!feedbackProcessors.ContainsKey(category))
            //    this.feedbackProcessors[category] = processor;
            //else
            //    this.feedbackProcessors[category] += processor;

            if (!this.feedbackProcessors.TryAdd(category, processor))
                this.feedbackProcessors[category] += processor;
        }
    }
}
