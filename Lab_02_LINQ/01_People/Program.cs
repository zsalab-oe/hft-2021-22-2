﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace _01_People
{
    static class MyExtension
    {
        /// <summary>
        /// Egy gyűjtemény elemeinek kiíratása a konzolra.
        /// Kiegészítő metódus (extension method). A "this" kulcsszó csak az első paraméter előtt szerepelhet!
        /// </summary>
        /// <typeparam name="T">A gyűjtemény elemeinek a típusa.</typeparam>
        /// <param name="input">A gyűjtemény, amin a metódus dolgozik.</param>
        /// <param name="header">Egyéb szöveg, amit az elemek kiíratása előtt, amolyan fejlécként kiírunk a konzolra.</param>
        public static void ToConsole<T>(this IEnumerable<T> input, string header)
        {
            Console.WriteLine($"************ {header} ************");
            foreach (T item in input)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine($"**********************************");
            Console.ReadLine();
        }
    }

    /// <summary>
    /// Model, amiben tárolunk egy-egy xml "person" elemet (element / node).
    /// </summary>
    class Person
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Dept { get; set; }
        public string Rank { get; set; }
        public string Phone { get; set; }
        public string Room { get; set; }

        public static Person Parse(XElement node)
        {
            return new Person()
            {
                Name = node.Element("name")?.Value,
                Email = node.Element("email")?.Value,
                Dept = node.Element("dept")?.Value,
                Rank = node.Element("rank")?.Value,
                Phone = node.Element("phone")?.Value,
                Room = node.Element("room")?.Value,
            };
        }

        /// <summary>
        /// A megadott url-en lévő XML-t betölti egy gyűjteménybe, ami Person objektumokat tartalmaz.
        /// </summary>
        /// <param name="url">XML fájl elérési útvonala, ami lehet URL is.</param>
        /// <returns>Person objektumokat tartalmazó gyűjtemény.</returns>
        public static IEnumerable<Person> Load(string url)
        {
            XDocument XDoc = XDocument.Load(url);
            // Descendants("..."): ilyen nevű gyerekelemek(gyerekelemek gyerekei is)
            return XDoc.Descendants("person").Select(node => Person.Parse(node));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<Person> people = Person.Load("http://users.nik.uni-obuda.hu/zsalab/hft/people.xml");
            people.Select(person => person.Name).ToConsole("ALL WORKERS");

            /* 1. number of AII workers (Alkalmazott Informatikai Intézet) */
            string dept = "Alkalmazott Informatikai Intézet";
            int num = people.Where(person => person.Dept == dept).Count();
            int num2 = people.Count(person => person.Dept == dept);
            Console.WriteLine("1. number of AII workers (Alkalmazott Informatikai Intézet):");
            Console.WriteLine(num);
            Console.WriteLine(num2);
            Console.ReadLine();


            /* 2. paginated list of AII workers */
            int current = 0;
            int pageSize = 15;
            while (current < num)
            {
                var q2 = people
                    .Where(person => person.Dept == dept)
                    .Skip(current)
                    .Take(pageSize)
                    .Select(person => person.Name);
                q2.ToConsole($"2. paginated list of AII workers: page {(current / pageSize) + 1} / {Math.Ceiling((double)num / pageSize)}");
                current += pageSize;
            }


            /* 3. people with the longest/shortest name */
            // lekérdező szintaxis (query syntax)
            // let: lokális változó deklarálása, ami csak a lekérdezésen belül elérhető
            var q3 = from person in people
                     let minLen = people.Min(person => person.Name.Length)
                     let maxLen = people.Max(person => person.Name.Length)
                     where person.Name.Length == minLen || person.Name.Length == maxLen
                     select new { person.Name, person.Name.Length };    // névtelen típus
            q3.ToConsole("3. people with the longest/shortest name");

            // névtelen típusú objektumnak ugyanúgy lekérhetők a tulajdonságaik
            string nameOfFirstPerson = q3.FirstOrDefault()?.Name;


            /* 4. number of people per department */
            var q4 = from person in people
                     group person by person.Dept into g
                     select new { Dept = g.Key, Count = g.Count() };
            // Key (csoport kulcsa): "mi alapján csoportosítottunk"
            q4.ToConsole("4. number of people per department");


            /* 5. biggest department */
            var q5 = q4
                .OrderByDescending(d => d.Count)
                .FirstOrDefault();
            Console.WriteLine($"************ 5. biggest department ************");
            Console.WriteLine(q5.ToString());


            Console.ReadLine();
        }
    }
}
