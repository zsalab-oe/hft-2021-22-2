﻿using System;
using ValidationExercise.Core;

namespace ValidationExercise.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Cake validCake = new Cake()
            {
                Name = "Rákóczi túrós",
                Price = 350
            };

            Cake invalidCake1 = new Cake()
            {
                Name = "Cukormentes csokitorta mazsolával és mandulával",
                Price = 390
            };

            Cake invalidCake2 = new Cake()
            {
                Name = "Napraforgó",
                Price = 990
            };


            // !!! using ValidationExercise.Core vs System.ComponentModel.DataAnnotations
            Validator validator = new Validator();
            Console.WriteLine("1st cake: " + validator.Validate(validCake));
            Console.WriteLine("2nd cake: " + validator.Validate(invalidCake1));
            Console.WriteLine("3rd cake: " + validator.Validate(invalidCake2));
            Console.ReadLine();
        }
    }
}
