﻿using System;
using ValidationExercise.Core;

namespace ValidationExercise.App
{
    class Cake
    {
        // Attribútum használatánál az osztály nevének végéről lehagyható az "Attribute" rész
        // !!! using ValidationExercise.Core vs System.ComponentModel.DataAnnotations
        [MaxLength(20)]
        public string Name { get; set; }

        [Range(50, 600)]
        public int Price { get; set; }
    }
}
