﻿using System;
using System.Reflection;

namespace ValidationExercise.Core
{
    internal class RangeValidation : IValidation
    {
        RangeAttribute range;
        public RangeValidation(RangeAttribute range)
        {
            this.range = range;
        }
        public bool Validate(object instance, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType != typeof(int))
                throw new InvalidOperationException();

            var value = (int)propertyInfo.GetValue(instance);
            return value >= range.Min && value <= range.Max;
        }
    }
}
