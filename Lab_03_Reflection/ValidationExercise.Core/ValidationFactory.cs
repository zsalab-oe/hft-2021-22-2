﻿using System;

namespace ValidationExercise.Core
{
    internal class ValidationFactory
    {
        /// <summary>
        /// Feladata, hogy létrehozzon példányokat. Jelen esetben IValidation-t implementáló példányokat, a paraméterben megadott Attribute alapján.
        /// Factory osztály felelősségi köre csak annyi, hogy példányt hozzon létre. Mivel a példánylétrehozás általában bonyolult művelet (konstruktorparaméterek, azok konstruktorparaméterei stb.), ezért ezeket célszerű kiemelni egy Factory osztályba.
        /// </summary>
        /// <param name="attribute">Attribútum, ami alapján eldöntésre kerül, pontosan milyen típusú példány jöjjön létre.</param>
        /// <returns></returns>
        public IValidation GetValidation(Attribute attribute)
        {
            if (attribute is MaxLengthAttribute)
            {
                return new MaxLengthValidation((MaxLengthAttribute)attribute);
            }

            if (attribute is RangeAttribute)
            {
                return new RangeValidation((RangeAttribute)attribute);
            }

            // Nem tudunk létrehozni egyéb Attribute alapján példányt. Ez nem feltétlenül hiba, majd a hívónak kell lekezelnie, ha nullt kapott vissza.
            return null;
        }
    }
}
