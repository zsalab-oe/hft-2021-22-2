﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ValidationExercise.Core
{
    public class Validator
    {
        public bool Validate(object instance)
        {
            ValidationFactory validationFactory = new ValidationFactory();

            PropertyInfo[] properties = instance.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in properties)
            {
                // GetCustomAttributes() vs GetCustomAttribute<>
                IEnumerable<Attribute> allAttributes = propertyInfo.GetCustomAttributes();
                foreach (Attribute attr in allAttributes)
                {
                    IValidation validation = validationFactory.GetValidation(attr);
                    if (validation?.Validate(instance, propertyInfo) == false)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
