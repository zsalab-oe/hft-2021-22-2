﻿using System;

namespace ValidationExercise.Core
{
    [AttributeUsage(AttributeTargets.Property)]     // megszorítás: csak tulajdonságra helyezhető attribútum
    public class MaxLengthAttribute : Attribute
    {
        public int Length { get; set; }

        public MaxLengthAttribute(int length)
        {
            this.Length = length;
        }
    }
}
