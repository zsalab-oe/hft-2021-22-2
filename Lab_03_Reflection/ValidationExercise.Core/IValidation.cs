﻿using System.Reflection;

namespace ValidationExercise.Core
{
    internal interface IValidation
    {
        bool Validate(object instance, PropertyInfo propertyInfo);
    }
}
