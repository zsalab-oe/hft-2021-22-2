﻿using System;

namespace ValidationExercise.Core
{
    [AttributeUsage(AttributeTargets.Property)]     // megszorítás: csak tulajdonságra helyezhető attribútum
    public class RangeAttribute : Attribute
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public RangeAttribute(int min = int.MinValue, int max = int.MaxValue)
        {
            Min = min;
            Max = max;
        }
    }
}
