﻿using System;
using System.Reflection;

namespace ValidationExercise.Core
{
    internal class MaxLengthValidation : IValidation
    {
        MaxLengthAttribute maxLength;
        public MaxLengthValidation(MaxLengthAttribute maxLength)
        {
            this.maxLength = maxLength;
        }
        public bool Validate(object instance, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType != typeof(string))
                throw new InvalidOperationException();

            var value = (string)propertyInfo.GetValue(instance);
            return value.Length <= maxLength.Length;

        }
    }
}
